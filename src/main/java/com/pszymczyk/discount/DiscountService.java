package com.pszymczyk.discount;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.pszymczyk.discount.User.Type.VIP;

public class DiscountService {

    public Collection<Discount> getSpecialDiscount(User user) {
        Transaction transaction = SessionService.getInstance().getCurrentTransaction();

        if (transaction.isOpen()) {
            if (user.getType() == VIP) {
                Collection<User> connectedAccounts = UserDao.getInstance().getConnectedAccounts(user.getId());
                List<Discount> discounts = new ArrayList<>();
                discounts.add(user.getDiscount(transaction.getBasket()));
                for (User connectedAccount: connectedAccounts) {
                    discounts.add(connectedAccount.getDiscount(transaction.getBasket()));
                }
                return discounts;
            } else {
                return Collections.singletonList(user.getDiscount(transaction.getBasket()));
            }
        } else {
            throw new ClosedTransactionException();
        }
    }
}
