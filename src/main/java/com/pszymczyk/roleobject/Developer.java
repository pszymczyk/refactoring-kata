package com.pszymczyk.roleobject;

import java.util.Collections;
import java.util.List;

public class Developer extends PersonRole {

    public List<String> programingLanguages() {
        return Collections.singletonList("Java");
    };

    @Override
    public boolean hasType(String roleName) {
        return "developer".equalsIgnoreCase(roleName);
    }
}
