package com.pszymczyk.roleobject;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class Person {

    private String id;
    private final List<PersonRole> roles;

    public Person() {
        this.roles = new ArrayList<>();
    }

    public void addRole(PersonRole value) {
        roles.add(value);
    }

    public boolean hasRole(String roleName) {
        return roles.stream()
                .anyMatch(role -> role.hasType(roleName));

    }

    public <T extends PersonRole> T roleOf(String roleName, Class<T> tClass) {
        return roles.stream()
             .filter(role -> role.hasType(roleName))
             .map(role -> tClass.cast(role))
             .findAny()
             .orElseThrow(() -> new CouldNotFindGivenRole(this.id, roleName));
    }
}
