package com.pszymczyk.roleobject;

public abstract class PersonRole {

    public abstract boolean hasType(String roleName);

}
