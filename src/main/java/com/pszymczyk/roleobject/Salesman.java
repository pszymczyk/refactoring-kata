package com.pszymczyk.roleobject;

public class Salesman extends PersonRole {

    public int numberOfSales () {
        return 99882;
    };

    @Override
    public boolean hasType(String roleName) {
        return "salesman".equalsIgnoreCase(roleName);
    }
}
