package com.pszymczyk.scoreboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class Game {

    private Collection<User> players = new ArrayList<>();
    private Map<Pocket, Set<Integer>> table = new HashMap<>();
    private User stripesPlayer;
    private User solidsPlayer;
    private Long id;

    public Collection<User> getPlayers() {
        return players;
    }

    public void setPlayers(Collection<User> players) {
        this.players = players;
    }

    public Map<Pocket, Set<Integer>> getTable() {
        return table;
    }

    public void setTable(Map<Pocket, Set<Integer>> table) {
        this.table = table;
    }

    public User getStripesPlayer() {
        return stripesPlayer;
    }

    public void setStripesPlayer(User stripesPlayer) {
        this.stripesPlayer = stripesPlayer;
    }

    public User getSolidsPlayer() {
        return solidsPlayer;
    }

    public void setSolidsPlayer(User solidsPlayer) {
        this.solidsPlayer = solidsPlayer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
