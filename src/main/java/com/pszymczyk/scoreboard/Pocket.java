package com.pszymczyk.scoreboard;

public enum  Pocket {

    LT_POCKET,
    RT_POCKET,
    LM_POCKET,
    RM_POCKET,
    LB_POCKET,
    RB_POCKET
}
