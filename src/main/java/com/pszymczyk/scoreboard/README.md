Projekt którym się aktualnie zajmujesz to portal do gry onnline w bilard. 


1. Aktualnie na portalu można grać tylko w tzw ["ósemkę"](https://en.wikipedia.org/wiki/Pool_(cue_sports)#Eight-ball), 
jednak ze względu na sukces oraz wysoką popularność platformy kolejnym wymaganiem 
jest rozbudowanie portalu o możliwość gry w snookera. Zacznij od modułu wyświetlającego aktualną ilość punktów gracza - `scoreboard`. 
Moduł ten oblicza i zwraca ilość punktów pozostałą na stole dla danego gracza
w danej grze. Ilość punktów obliczana jest zgodnie z regułami gry, ilość punktów może
być sprawdzona tylko przez gracza lub uzytkownika zaprzyjaźnionego z graczem.

2. W związku z brakiem możliwości interwencji podczas awarii (podejżenia aktualnego 
stanu gry) przez pracowników z obsługi należy dodać nowy typ użytkownika `Administrator`
który to będzie mógł podejrzeć stan dowolnej gry. Administrator może też być graczem,
ale sama rola aministratora nie ma możliwości dodawania znajomych w systemie, natomiast 
administrator może dodawać do ulubionych gry które wydająmu się podejrzane.   

