package com.pszymczyk.scoreboard;

import java.util.Set;

public class ScoreboardService {

    public int getUserResult(User user) {
        Game game = SessionService.getInstance().getCurrentGame();
        if (game != null) {
            if (game.getPlayers().contains(user)) {
                int pointsLeft = 6;
                for (Set<Integer> pocket : game.getTable().values()) {
                    for (int ball : pocket) {
                        boolean isStripesPlayer = user.equals(game.getStripesPlayer());
                        if (ball > 8 && ball <= 15 && isStripesPlayer) {
                            pointsLeft--;
                        } else {
                            boolean isSolidPlayer = user.equals(game.getSolidsPlayer());
                            if (ball >= 1 && ball <= 7 && isSolidPlayer) {
                                pointsLeft--;
                            } else if (ball == 8) {
                                return 0;
                            } else {
                                throw new UnknownBallException();
                            }
                        }
                    }
                }
                return pointsLeft;
            } else {
                boolean isFriend = false;

                for (User friend : user.getFriends()) {
                    if (game != null && game.getPlayers().contains(friend)) {
                        user = friend;
                        isFriend = true;
                        break;
                    }
                }

                if (isFriend) {
                    int pointsLeft = 6;
                    for (Set<Integer> pocket : game.getTable().values()) {
                        for (int ball : pocket) {
                            if (ball > 8 && ball <= 15 && user.equals(game.getStripesPlayer())) {
                                pointsLeft--;
                            } else if (ball >= 1 && ball <= 7 && user.equals(game.getSolidsPlayer())) {
                                pointsLeft--;
                            } else if (ball == 8) {
                                return 0;
                            } else {
                                throw new UnknownBallException();
                            }
                        }
                    }
                    return pointsLeft;
                } else {
                    throw new InsufficientPermissionsException();
                }
            }
        } else {
            throw new IllegalStateException("Game should go on here!");
        }
    }
}