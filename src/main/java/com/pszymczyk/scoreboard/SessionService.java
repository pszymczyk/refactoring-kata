package com.pszymczyk.scoreboard;

class SessionService {

    static SessionService getInstance() {
        throw new UnsupportedOperationException("Should not be called during unit tests");
    }

    Game getCurrentGame() {
        throw new UnsupportedOperationException("Should not be called during unit tests");
    }

    User getLoggedUser() {
        throw new UnsupportedOperationException("Should not be called during unit tests");
    }
}
