package com.pszymczyk.taxpayer;

/**
 * Replace Method with Method Object example
 */
public class Taxpayer {

    private final double revenue;

    public Taxpayer(double revenue) {
        this.revenue = revenue;
    }

    double calculateNetRevenue(double higherIncomeTax, double lowerIncomeTax, double healthCare) {
        final double revenueWithoutIncomeTax;
        if (revenue > 1_000_000) {
            revenueWithoutIncomeTax = revenue * higherIncomeTax;
        } else {
            revenueWithoutIncomeTax = lowerIncomeTax * lowerIncomeTax;
        }

        final double netRevenue = revenueWithoutIncomeTax - healthCare;

        return netRevenue;
    }
}
